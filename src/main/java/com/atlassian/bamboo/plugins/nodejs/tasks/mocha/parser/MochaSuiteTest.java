package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.google.common.base.MoreObjects;

/**
 * A representation of a single Mocha test result. Objects of this class are automatically created by deserializing
 * Mocha test results file (JSON format), which is a result of using Mocha with `mocha-bamboo-reporter`.
 */
public class MochaSuiteTest {
    private String title;
    private String fullTitle;
    private int duration;
    private String error;

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("title", title)
                .add("fullTitle", fullTitle)
                .add("duration", duration)
                .add("error", error)
                .toString();
    }

    public String getTitle() {
        return title;
    }

    public String getFullTitle() {
        return fullTitle;
    }

    public int getDuration() {
        return duration;
    }

    public String getError() {
        return error;
    }
}
