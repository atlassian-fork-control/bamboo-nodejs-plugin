package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.bower.BowerConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;

import java.util.Map;

public class BowerTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent {
    public static final String TASK_NAME = "Bower";

    @ElementBy(name = BowerConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = BowerConfigurator.BOWER_RUNTIME)
    private TextElement bowerRuntimeField;

    @ElementBy(name = BowerConfigurator.COMMAND)
    private TextElement commandField;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @Override
    public void updateTaskDetails(Map<String, String> config) {
        this.withAdvancedOptions();

        if (config.containsKey(BowerConfigurator.NODE_RUNTIME)) {
            nodeRuntimeField.select(Options.value(config.get(BowerConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(BowerConfigurator.BOWER_RUNTIME)) {
            bowerRuntimeField.setText(config.get(BowerConfigurator.BOWER_RUNTIME));
        }
        if (config.containsKey(BowerConfigurator.COMMAND)) {
            commandField.setText(config.get(BowerConfigurator.COMMAND));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)) {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)) {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }
}