package com.atlassian.bamboo.plugins.nodejs.tasks.gulp;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.GulpTask;
import com.atlassian.bamboo.specs.model.task.GulpTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GulpTaskExporter extends AbstractNodeRequiringTaskExporter<GulpTaskProperties, GulpTask> {
    @Autowired
    public GulpTaskExporter(UIConfigSupport uiConfigSupport) {
        super(GulpTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return GulpTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull GulpTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(GulpConfigurator.GULP_RUNTIME, taskProperties.getGulpExecutable());
        config.put(GulpConfigurator.TASK, taskProperties.getTask());
        config.put(GulpConfigurator.CONFIG_FILE, taskProperties.getGulpfile());
        return config;
    }

    @NotNull
    @Override
    protected GulpTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new GulpTask()
                .gulpExecutable(taskConfiguration.get(GulpConfigurator.GULP_RUNTIME))
                .task(taskConfiguration.getOrDefault(GulpConfigurator.TASK, null))
                .gulpfile(taskConfiguration.getOrDefault(GulpConfigurator.CONFIG_FILE, null));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull GulpTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isBlank(taskProperties.getGulpExecutable())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Gulp executable is not defined"));
        }
        return validationProblems;
    }
}
