package com.atlassian.bamboo.plugins.nodejs.tasks.bower;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.BowerTask;
import com.atlassian.bamboo.specs.model.task.BowerTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BowerTaskExporter extends AbstractNodeRequiringTaskExporter<BowerTaskProperties, BowerTask> {
    @Autowired
    public BowerTaskExporter(UIConfigSupport uiConfigSupport) {
        super(BowerTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return BowerTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull BowerTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(BowerConfigurator.BOWER_RUNTIME, taskProperties.getBowerExecutable());
        config.put(BowerConfigurator.COMMAND, taskProperties.getCommand());
        return config;
    }

    @NotNull
    @Override
    protected BowerTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new BowerTask()
                .bowerExecutable(taskConfiguration.get(BowerConfigurator.BOWER_RUNTIME))
                .command(taskConfiguration.get(BowerConfigurator.COMMAND));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull BowerTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isBlank(taskProperties.getBowerExecutable())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Bower executable is not defined"));
        }
        if (StringUtils.isBlank(taskProperties.getCommand())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Command is not defined"));
        }
        return validationProblems;
    }
}
