package com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeCapabilityDefaultsHelper;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NodeunitTask;
import com.atlassian.bamboo.specs.model.task.NodeunitTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import com.google.common.collect.ImmutableMap;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NodeunitTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private UIConfigSupport uiConfigSupport;

    @InjectMocks
    private NodeunitTaskExporter nodeunitTaskExporter;

    @Test
    public void testToTaskConfiguration() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String nodeunitExecutable = "custom/node_modules/nodeunit";
        final String testFilesAndDirectories = "test.js tests/";
        final String testResultsDirectory = "nodeunit-results/";
        final boolean parseTestResults = true;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final NodeunitTask nodeunitTask = new NodeunitTask()
                .nodeExecutable(nodeExecutable)
                .nodeunitExecutable(nodeunitExecutable)
                .testFilesAndDirectories(testFilesAndDirectories)
                .testResultsDirectory(testResultsDirectory)
                .parseTestResults(parseTestResults)
                .arguments(arguments)
                .environmentVariables(environmentVariables)
                .workingSubdirectory(workingSubdirectory);
        final NodeunitTaskProperties nodeunitTaskProperties = EntityPropertiesBuilders.build(nodeunitTask);

        // when
        final Map<String, String> configuration = nodeunitTaskExporter.toTaskConfiguration(taskContainer, nodeunitTaskProperties);

        // then
        assertThat(configuration.get(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME), is(nodeExecutable));
        assertThat(configuration.get(NodeunitConfigurator.NODEUNIT_RUNTIME), is(nodeunitExecutable));
        assertThat(configuration.get(NodeunitConfigurator.TEST_FILES), is(testFilesAndDirectories));
        assertThat(configuration.get(NodeunitConfigurator.TEST_RESULTS_DIRECTORY), is(testResultsDirectory));
        assertThat(configuration.get(NodeunitConfigurator.PARSE_TEST_RESULTS), is(Boolean.toString(parseTestResults)));
        assertThat(configuration.get(NodeunitConfigurator.ARGUMENTS), is(arguments));
        assertThat(configuration.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES), is(environmentVariables));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
    }

    @Test
    public void testToSpecsEntity() {
        // provided
        final String nodeExecutable = "Node.js 6.0";
        final String nodeunitExecutable = "custom/node_modules/nodeunit";
        final String testFilesAndDirectories = "test.js tests/";
        final String testResultsDirectory = "nodeunit-results/";
        final boolean parseTestResults = true;
        final String arguments = "--run-fast";
        final String environmentVariables = "NODE_HOME=/env/home";
        final String workingSubdirectory = "plugin";

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(AbstractNodeRequiringTaskConfigurator.NODE_RUNTIME, nodeExecutable)
                .put(NodeunitConfigurator.NODEUNIT_RUNTIME, nodeunitExecutable)
                .put(NodeunitConfigurator.TEST_FILES, testFilesAndDirectories)
                .put(NodeunitConfigurator.TEST_RESULTS_DIRECTORY, testResultsDirectory)
                .put(NodeunitConfigurator.PARSE_TEST_RESULTS, Boolean.toString(parseTestResults))
                .put(NodeunitConfigurator.ARGUMENTS, arguments)
                .put(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES, environmentVariables)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final NodeunitTaskProperties taskProperties = EntityPropertiesBuilders.build(nodeunitTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getNodeExecutable(), is(nodeExecutable));
        assertThat(taskProperties.getNodeunitExecutable(), is(nodeunitExecutable));
        assertThat(taskProperties.getTestFilesAndDirectories(), is(testFilesAndDirectories));
        assertThat(taskProperties.getTestResultsDirectory(), is(testResultsDirectory));
        assertThat(taskProperties.isParseTestResults(), is(parseTestResults));
        assertThat(taskProperties.getArguments(), is(arguments));
        assertThat(taskProperties.getEnvironmentVariables(), is(environmentVariables));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
    }

    @Test
    public void testValidatePassesForValidProperties() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};

        final NodeunitTask nodeunitTask = new NodeunitTask().nodeExecutable(knownNodeExecutables[0]);
        final NodeunitTaskProperties nodeunitTaskProperties = EntityPropertiesBuilders.build(nodeunitTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = nodeunitTaskExporter.validate(taskValidationContext, nodeunitTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testValidateFailsForUnknownExecutable() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);
        final String[] knownNodeExecutables = {"Node.js 0.11", "Node.js 0.12"};
        final String missingNodeExecutable = knownNodeExecutables[0] + " (missing)";
        final NodeunitTask nodeunitTask = new NodeunitTask().nodeExecutable(missingNodeExecutable);
        final NodeunitTaskProperties nodeunitTaskProperties = EntityPropertiesBuilders.build(nodeunitTask);
        registerKnownServerExecutables(knownNodeExecutables);

        // when
        final List<ValidationProblem> validationProblems = nodeunitTaskExporter.validate(taskValidationContext, nodeunitTaskProperties);

        // then
        assertThat(validationProblems, hasSize(1));
    }

    private void registerKnownServerExecutables(@NotNull String... labels) {
        when(uiConfigSupport.getExecutableLabels(NodeCapabilityDefaultsHelper.NODE_CAPABILITY_KEY)).thenReturn(Arrays.asList(labels));
    }
}
