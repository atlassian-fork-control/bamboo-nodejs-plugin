package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.tasks.ScriptTaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner.MochaRunnerConfigurator;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.task.ScriptTaskDetails;
import com.atlassian.bamboo.testutils.model.task.ScriptTaskDetails.ScriptLocation;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.MochaParserTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.MochaRunnerTaskComponent;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NpmTaskComponent;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class MochaTaskTest extends AbstractNodeTaskTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Override
    protected void onSetUp() {
    }

    @Override
    protected void onTearDown() {
    }

    @Test
    public void testMochaRunnerWithParsingEnabled() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();
        final TestBuildDetails plan = createPlanWithNpmModulesInstalled();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // add Mocha runner task
        final Map<String, String> mochaRunnerTaskConfig = ImmutableMap.of(
                MochaRunnerConfigurator.TEST_FILES, "test/mocha/",
                MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.TRUE.toString());
        taskConfigurationPage.addNewTask(MochaRunnerTaskComponent.TASK_NAME, MochaRunnerTaskComponent.class, null, mochaRunnerTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 1), is(1));
    }

    @Test
    public void testMochaRunnerWithMochaParser() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();
        final TestBuildDetails plan = createPlanWithNpmModulesInstalled();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // add Mocha runner task
        final Map<String, String> mochaRunnerTaskConfig = ImmutableMap.of(
                MochaRunnerConfigurator.TEST_FILES, "test/mocha/",
                MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.FALSE.toString());
        taskConfigurationPage.addNewTask(MochaRunnerTaskComponent.TASK_NAME, MochaRunnerTaskComponent.class, null, mochaRunnerTaskConfig);

        // assert that with parsing disabled we don't see any test results
        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);

        assertThat("Shouldn't have found any test results", getTotalNumberOfTestsForBuild(plan.getKey(), 1), is(0));

        // add Mocha parser task
        final Map<String, String> mochaParserTaskConfig = ImmutableMap.of();
        taskConfigurationPage.addNewTask(MochaParserTaskComponent.TASK_NAME, MochaParserTaskComponent.class, null, mochaParserTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 2);

        assertThat("Should have found 1 successful test", getNumberOfSuccessfulTestsForBuild(plan.getKey(), 2), is(1));
    }

    @Test
    public void testMochaParserPicksUpOutdatedFiles() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();
        final TestBuildDetails plan = createPlanWithNpmModulesInstalled();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        createMochaReportInWorkingDirectory(taskConfigurationPage, "mocha.json");

        // mock test report creation date
        final Map<String, String> mockDateTaskConfig = generateScriptTaskConfig("touch -t 201201010000 mocha.json");
        taskConfigurationPage.addNewTask(ScriptTaskComponent.getName(), ScriptTaskComponent.class, "mock report creation date", mockDateTaskConfig);

        // add Mocha parser task without picking outdated files
        final Map<String, String> mochaParserTaskConfig = Collections.emptyMap();
        final String mochaParserTaskDescription = "parse results";
        taskConfigurationPage.addNewTask(MochaParserTaskComponent.TASK_NAME, MochaParserTaskComponent.class, mochaParserTaskDescription, mochaParserTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForCompletedBuild(plan.getKey(), 1);
        final boolean firstBuildSuccessful = backdoor.plans().getBuildResult(plan.getKey(), 1).isSuccessful();
        assertThat(firstBuildSuccessful, is(false));

        // modify mocha parser task to pick outdated files
        final Map<String, String> updatedMochaParserTaskConfig = ImmutableMap.of(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, "true");
        taskConfigurationPage.editTask(mochaParserTaskDescription, MochaParserTaskComponent.class, mochaParserTaskDescription, updatedMochaParserTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 2);
    }

    @Test
    public void testMochaParserUsesWorkingDirectoryField() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();
        final TestBuildDetails plan = createPlanWithNpmModulesInstalled();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        createMochaReportInWorkingDirectory(taskConfigurationPage, "mocha.json");

        // script task to be updated later, to move the report file to a different directory
        final String moreReportFileScriptName = "move report file";
        taskConfigurationPage.addNewTask(ScriptTaskComponent.getName(), ScriptTaskComponent.class, moreReportFileScriptName, generateScriptTaskConfig(""));

        // add Mocha parser task with subdirectory set
        final Map<String, String> mochaParserTaskConfig = ImmutableMap.of(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY, "subdirectory");
        taskConfigurationPage.addNewTask(MochaParserTaskComponent.TASK_NAME, MochaParserTaskComponent.class, "parse results", mochaParserTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForCompletedBuild(plan.getKey(), 1);
        final boolean firstBuildSuccessful = backdoor.plans().getBuildResult(plan.getKey(), 1).isSuccessful();
        assertThat(firstBuildSuccessful, is(false));

        // move the report to the subdirectory
        final Map<String, String> moveReportTaskConfig = generateScriptTaskConfig("mkdir -p subdirectory && mv mocha.json subdirectory");
        taskConfigurationPage.editTask(moreReportFileScriptName, ScriptTaskComponent.class, moreReportFileScriptName, moveReportTaskConfig);

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 2);
    }

    @NotNull
    private TestBuildDetails createPlanWithNpmModulesInstalled() throws Exception {
        final TestBuildDetails plan = createAndSetupPlan();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, plan.getDefaultJob());

        // install required modules using npm
        final Map<String, String> npmTaskConfig = ImmutableMap.of(NpmConfigurator.COMMAND, "install mocha mocha-bamboo-reporter");
        taskConfigurationPage.addNewTask(NpmTaskComponent.TASK_NAME, NpmTaskComponent.class, "npm install", npmTaskConfig);

        return plan;
    }

    private void createMochaReportInWorkingDirectory(@NotNull JobTaskConfigurationPage taskConfigurationPage, @NotNull String reportFileName) throws Exception {
        // copy report content to temporary file
        final File temporaryFile = temporaryFolder.newFile();
        try (final InputStream reportFileInputStream = getClass().getResourceAsStream("/mocha.json")) {
            Files.copy(reportFileInputStream, temporaryFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }

        // copy report file from temporary folder to working directory
        final Map<String, String> produceReportTaskConfig = generateScriptTaskConfig(String.format("cp %s %s", temporaryFile.getCanonicalPath(), reportFileName));
        taskConfigurationPage.addNewTask(ScriptTaskComponent.getName(), ScriptTaskComponent.class, "produce report", produceReportTaskConfig);
    }

    /**
     * Returns config of a {@link ScriptTaskComponent} that will execute the given script body.
     * <p>
     * Using temporary file as a workaround solution instead of inline script, as {@link ScriptTaskComponent} does not
     * properly handle all characters (e.g. the hyphen).
     */
    @NotNull
    private Map<String, String> generateScriptTaskConfig(@NotNull String scriptBody) throws Exception {
        final File temporaryFile = temporaryFolder.newFile();
        FileUtils.write(temporaryFile, scriptBody);

        return ImmutableMap.of(
                ScriptTaskComponent.CONFIG_INTERPRETER, ScriptTaskDetails.Interpreter.LEGACY_SH_BAT.name(),
                ScriptTaskComponent.CONFIG_SCRIPT_LOCATION, ScriptLocation.FILE.name(),
                ScriptTaskComponent.CONFIG_SCRIPT_FILE, temporaryFile.getCanonicalPath());
    }
}
