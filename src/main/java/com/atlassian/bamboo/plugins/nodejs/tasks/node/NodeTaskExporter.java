package com.atlassian.bamboo.plugins.nodejs.tasks.node;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.NodeTask;
import com.atlassian.bamboo.specs.model.task.NodeTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NodeTaskExporter extends AbstractNodeRequiringTaskExporter<NodeTaskProperties, NodeTask> {
    @Autowired
    public NodeTaskExporter(UIConfigSupport uiConfigSupport) {
        super(NodeTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return NodeTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull NodeTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(NodeConfigurator.COMMAND, taskProperties.getScript());
        config.put(NodeConfigurator.ARGUMENTS, taskProperties.getArguments());
        return config;
    }

    @NotNull
    @Override
    protected NodeTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new NodeTask()
                .script(taskConfiguration.get(NodeConfigurator.COMMAND))
                .arguments(taskConfiguration.getOrDefault(NodeConfigurator.ARGUMENTS, null));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull NodeTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isBlank(taskProperties.getScript())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Script is not defined"));
        }
        return validationProblems;
    }
}
