package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.pageobjects.utils.FormUtils;
import com.atlassian.bamboo.plugins.nodejs.tasks.nodeunit.NodeunitConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;

import java.util.Map;

public class NodeunitTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent {
    public static final String TASK_NAME = "Nodeunit";

    @ElementBy(name = NodeunitConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = NodeunitConfigurator.NODEUNIT_RUNTIME)
    private TextElement nodeunitRuntimeField;

    @ElementBy(name = NodeunitConfigurator.TEST_FILES)
    private TextElement testFilesField;

    @ElementBy(name = NodeunitConfigurator.TEST_RESULTS_DIRECTORY)
    private TextElement testResultsDirField;

    @ElementBy(name = NodeunitConfigurator.PARSE_TEST_RESULTS)
    private CheckboxElement parseTestResultsCheckbox;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @Override
    public void updateTaskDetails(Map<String, String> config) {
        this.withAdvancedOptions();

        if (config.containsKey(NodeunitConfigurator.NODE_RUNTIME)) {
            nodeRuntimeField.select(Options.value(config.get(NodeunitConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(NodeunitConfigurator.NODEUNIT_RUNTIME)) {
            nodeunitRuntimeField.setText(config.get(NodeunitConfigurator.NODEUNIT_RUNTIME));
        }
        if (config.containsKey(NodeunitConfigurator.TEST_FILES)) {
            testFilesField.setText(config.get(NodeunitConfigurator.TEST_FILES));
        }
        if (config.containsKey(NodeunitConfigurator.TEST_RESULTS_DIRECTORY)) {
            testResultsDirField.setText(config.get(NodeunitConfigurator.TEST_RESULTS_DIRECTORY));
        }
        if (config.containsKey(NodeunitConfigurator.PARSE_TEST_RESULTS)) {
            FormUtils.checkCheckbox(parseTestResultsCheckbox, Boolean.parseBoolean(config.get(NodeunitConfigurator.PARSE_TEST_RESULTS)));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)) {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)) {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }
}
