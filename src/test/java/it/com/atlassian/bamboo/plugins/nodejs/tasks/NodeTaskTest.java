package it.com.atlassian.bamboo.plugins.nodejs.tasks;

import com.atlassian.bamboo.pageobjects.pages.plan.configuration.ArtifactConfigurationPage;
import com.atlassian.bamboo.pageobjects.pages.plan.configuration.JobTaskConfigurationPage;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeConfigurator;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.model.TestJobDetails;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.bamboo.plugins.nodejs.pageobjects.NodeTaskComponent;
import org.junit.Test;

import java.io.File;
import java.util.Map;

public class NodeTaskTest extends AbstractNodeTaskTest {
    @Override
    protected void onSetUp() {
    }

    @Override
    protected void onTearDown() {
    }

    @Test
    public void testTask() throws Exception {
        product.gotoLoginPage().loginAsSysAdmin();

        final TestBuildDetails plan = createAndSetupPlan();
        final TestJobDetails defaultJob = plan.getDefaultJob();
        final JobTaskConfigurationPage taskConfigurationPage = product.visit(JobTaskConfigurationPage.class, defaultJob);

        // add Node.js task
        final Map<String, String> nodeTaskConfig = ImmutableMap.of(NodeConfigurator.COMMAND, "src" + File.separator + "helloWorld.js");
        taskConfigurationPage.addNewTask(NodeTaskComponent.TASK_NAME, NodeTaskComponent.class, "Hello, World!", nodeTaskConfig);

        // add artifact definition
        final ArtifactConfigurationPage artifactConfigurationPage = product.visit(ArtifactConfigurationPage.class, defaultJob.getKey());
        artifactConfigurationPage.createArtifactDefinition("Hello World", "build", "helloWorld.txt");

        backdoor.plans().triggerBuild(plan.getKey());
        backdoor.plans().waitForSuccessfulBuild(plan.getKey(), 1);
    }

}