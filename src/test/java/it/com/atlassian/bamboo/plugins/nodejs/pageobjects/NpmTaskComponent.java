package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.pageobjects.utils.FormUtils;
import com.atlassian.bamboo.plugins.nodejs.tasks.npm.NpmConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.CheckboxElement;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;

import java.util.Map;

public class NpmTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent {
    public static final String TASK_NAME = "npm";

    @ElementBy(name = NpmConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = NpmConfigurator.COMMAND)
    private TextElement commandField;

    @ElementBy(name = NpmConfigurator.ISOLATED_CACHE)
    private CheckboxElement isolatedCacheCheckbox;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @Override
    public void updateTaskDetails(Map<String, String> config) {
        this.withAdvancedOptions();

        if (config.containsKey(NpmConfigurator.NODE_RUNTIME)) {
            nodeRuntimeField.select(Options.value(config.get(NpmConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(NpmConfigurator.COMMAND)) {
            commandField.setText(config.get(NpmConfigurator.COMMAND));
        }
        if (config.containsKey(NpmConfigurator.ISOLATED_CACHE)) {
            FormUtils.checkCheckbox(isolatedCacheCheckbox, Boolean.parseBoolean(config.get(NpmConfigurator.ISOLATED_CACHE)));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)) {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)) {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }

    public String getNodeExecutable() {
        return nodeRuntimeField.getSelected().text();
    }

    public String getCommand() {
        return commandField.getValue();
    }

    public boolean isUseIsolatedCache() {
        return isolatedCacheCheckbox.isChecked();
    }

    public String getEnvironmentVariables() {
        this.withAdvancedOptions();
        return environmentVariablesField.getValue();
    }

    public String getWorkingDirectory() {
        this.withAdvancedOptions();
        return workingSubDirectoryField.getValue();
    }
}
